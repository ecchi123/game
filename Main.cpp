#include "pch.h"
#include "Main.h"


Main::Main(int screenWidth, int screenHeight, int cloudWidth, int cloudHeight)
{
	quit = false;
	game_setup = new Game_setup(&quit, screenWidth, screenHeight);
	background = new Sprite(game_setup->GetRenderer(), "image/background4.png", 0, 0, screenWidth, screenHeight);
	cloud = new Cloud(game_setup, "image/cloud.png", CLOUD_START_X, CLOUD_START_Y, cloudWidth, cloudHeight);
	food = new Food(game_setup);
	threat = new Threat(game_setup);
	for (int i = 0; i < HEALTH_POINT; ++i)
	{
		heart[i] = new Sprite(game_setup->GetRenderer(), "image/heart.png", HEART_X + i * HEART_WIDTH, HEART_Y, HEART_WIDTH, HEART_HEIGHT);
		heart[i]->SetAmountFrame(2, 1);
		heart[i]->SetCurrentFrame(0);
		heart[i]->PlayAnimation(0, 0, 0, 0);
	}
	score_text = new Game_Text(game_setup, "font/FVF Fernando 08.ttf", 16);
	bestText = new Game_Text(game_setup, "font/FVF Fernando 08.ttf", 16);
	game_quit = new Sprite(game_setup->GetRenderer(), "image/X.png", X_LOCATION_X, X_LOCATION_Y, X_SIZE_WIDTH, X_SIZE_HEIGHT);
	game_over = new Sprite(game_setup->GetRenderer(), "image/game_over.png", 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	back_menu = new Game_Text(game_setup, "font/FVF Fernando 08.ttf", 16);
	back_menu->SetColor(69, 29, 220);
	back_menu->SetSize(100, 100);
	back_menu->SetLocation(900, 500);
	menu = new Menu(game_setup, "image/menu.jpg");
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		std::cout << Mix_GetError() << std::endl;
		std::cout << "failed to open sound device" << std::endl;
	}
	backgroundSound = Mix_LoadMUS("audio/background.mp3");
	if (backgroundSound == NULL)
	{
		std::cout << Mix_GetError() << std::endl;
		std::cout << "failed to load sound" << std::endl;
	}
	score_text->SetColor(0, 0, 0);
	score_text->SetSize(150, 50);
	score_text->SetLocation(800, 50);
	for (int i = 0; i < 10; ++i)
	{
		scoreStr[i] = '0';
	}
	timeCheck = SDL_GetTicks();
	slowTime = SDL_GetTicks();
	score = 0;
	playMusic = true;
	slowDown = false;
	select = Uncommand;
	healthPoint = 3;
	highScoreFile.open("bestScore.txt");
	highScoreFile >> bestScore;
}
Main::~Main()
{
	delete background;
	delete cloud;
	delete score_text;
	delete food;
	delete menu;
	for (int i = 0; i < HEALTH_POINT; ++i)
	{
		delete heart[i];
	}
	delete threat;
	delete game_quit;
	delete back_menu;
	delete game_over;
}

void Main::GameLoop()
{
	FirstSetup();
	PlayMusic(backgroundSound);
	Mix_VolumeMusic(MIX_MAX_VOLUME);
	Mix_Volume(-1, MIX_MAX_VOLUME);
	while (!quit && game_setup->GetMainEvent()->type != SDL_QUIT && select != Game_Quit)
	{
		game_setup->Begin();
		background->Draw();
		game_quit->Draw();
		CheckQuit();
		UpdateCondition();
		DrawHeart();
		cloud->Draw();
		cloud->Movement();
		threat->Draw();
		threat->Movement();
		food->Draw();
		food->Movement();
		IntergerToString(score);
		score_text->LoadText(scoreStr);
		score_text->RenderText();
		game_setup->End();
	}
}